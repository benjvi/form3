package com.benjvi.f3;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import au.com.dius.pact.provider.spring.SpringRestPactRunner;
import com.benjvi.f3.entity.Payment;
import com.benjvi.f3.persistence.PaymentRepository;
import com.benjvi.f3.rest.PaymentCollectionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.charset.Charset.defaultCharset;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;

@RunWith(SpringRestPactRunner.class)
@Provider("payments")
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@PactFolder("pacts")
public class PaymentServiceProviderPactTest {

    @Autowired
    PaymentRepository paymentRepository;

    @TestTarget
    public final Target target = new HttpTarget(8080);

    @State("pre-loaded payments")
    public void preLoadedPaymentsState() throws IOException, URISyntaxException {
        URI dataPath = getSystemResource("test-data.json").toURI();
        final String json = readAllLines(get(dataPath), defaultCharset())
                .stream().reduce("", String::concat);
        final ObjectMapper mapper = new ObjectMapper().registerModule(new Jdk8Module());
        final PaymentCollectionDTO readValue = mapper.readValue(json, PaymentCollectionDTO.class);
        List<Payment> payments = readValue.getPayments().stream().map(dto -> dto.toAggregate()).collect(toList());
        payments.get(0).setId("4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43");
        paymentRepository.save(payments);
    }

    @After
    public void tearDown() {
        paymentRepository.deleteAll();
    }

}
