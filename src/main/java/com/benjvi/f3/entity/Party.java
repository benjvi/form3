package com.benjvi.f3.entity;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Party {

    private String accountName;

    private String accountNumber;

    private String accountNumberCode;

    private Long accountType;

    private String address;

    private Long bankId;

    private String bankIdCode;

    private String name;

}
