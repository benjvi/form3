package com.benjvi.f3.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Embeddable
@Data
public class PaymentCharges {

    private String chargesBearerCode;

    private BigDecimal receiverChargesAmount;

    private String receiverChargesCurrency;

    @OneToMany(cascade = ALL, orphanRemoval = true, fetch = EAGER, mappedBy = "payment")
    private List<SenderPaymentCharge> senderCharges;

}
