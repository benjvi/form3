package com.benjvi.f3.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "sender_payment_charge")
@Data
public class SenderPaymentCharge {

    @Id
    @GeneratedValue(strategy = SEQUENCE)
    private Long id;

    @ManyToOne(cascade = PERSIST, fetch = LAZY)
    @JoinColumn(name = "payment_id", nullable = false)
    private Payment payment;

    private BigDecimal amount;

    private String currency;

}
