package com.benjvi.f3.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Embeddable
@Data
public class Forex {

    private String fxContractReference;

    private BigDecimal fxExchangeRate;

    private BigDecimal fxOriginalAmount;

    private String fxOriginalCurrency; // ~enum type

}
