package com.benjvi.f3.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "payment")
@Data
public class Payment {

    public Payment() {}

    public Payment(PaymentAttributes attributes, String organisationId) {
        this.attributes = attributes;
        this.version = 0L;
        this.type = "Payment";
        this.organisationId = organisationId;
        if (this.getAttributes() != null && this.getAttributes().getChargesInformation() != null
                && this.getAttributes().getChargesInformation().getSenderCharges() != null) {
            this.getAttributes().getChargesInformation().getSenderCharges().stream().forEach(c -> c.setPayment(this));
        }
    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "com.benjvi.f3.entity.PaymentUUIDGenerator")
    @Column(updatable = false)
    private String id;

    private String type;
    private Long version;

    private String organisationId;

    @Embedded
    private PaymentAttributes attributes;

}
