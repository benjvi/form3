package com.benjvi.f3.entity;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.UUIDGenerator;


import javax.persistence.PersistenceUnit;
import java.io.Serializable;

import static org.springframework.util.StringUtils.isEmpty;

public class PaymentUUIDGenerator extends UUIDGenerator {
    @Override
    public Serializable generate(SessionImplementor session, Object object)
            throws HibernateException {
        if (object instanceof Payment) {
            Payment persistent = (Payment)object;
            if (persistent != null && !isEmpty(persistent.getId())) {
                return persistent.getId();
            }
        }
        return super.generate(session, object);
    }
}
