package com.benjvi.f3.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Embeddable
@Data
public class PaymentAttributes {
    private BigDecimal amount;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "accountName", column = @Column(name = "beneficiaryAccountName")),
            @AttributeOverride(name = "accountNumber", column = @Column(name = "beneficiaryAccountNumber")),
            @AttributeOverride(name = "accountNumberCode", column = @Column(name = "beneficiaryAccountNumberCode")),
            @AttributeOverride(name = "accountType", column = @Column(name = "beneficiaryAccountType")),
            @AttributeOverride(name = "address", column = @Column(name = "beneficiaryAddress")),
            @AttributeOverride(name = "bankId", column = @Column(name = "beneficiaryBankId")),
            @AttributeOverride(name = "bankIdCode", column = @Column(name = "beneficiaryBankIdCode")),
            @AttributeOverride(name = "name", column = @Column(name = "beneficiaryName"))
    })
    private Party beneficiaryParty;

    @Embedded
    private PaymentCharges chargesInformation;

    private String currency;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "accountName", column = @Column(name = "debtorAccountName")),
            @AttributeOverride(name = "accountNumber", column = @Column(name = "debtorAccountNumber")),
            @AttributeOverride(name = "accountNumberCode", column = @Column(name = "debtorAccountNumberCode")),
            @AttributeOverride(name = "accountType", column = @Column(name = "debtorAccountType")),
            @AttributeOverride(name = "address", column = @Column(name = "debtorAddress")),
            @AttributeOverride(name = "bankId", column = @Column(name = "debtorBankId")),
            @AttributeOverride(name = "bankIdCode", column = @Column(name = "debtorBankIdCode")),
            @AttributeOverride(name = "name", column = @Column(name = "debtorName"))
    })
    private Party debtorParty;

    private String endToEndReference;

    @Embedded
    private Forex fx;

    private Long numericReference;

    private Long paymentId;

    private String paymentPurpose;

    private String paymentScheme;

    private String paymentType;

    @Temporal(TemporalType.DATE)
    private Date processingDate; // assuming consistent timezones

    private String reference;

    private String schemePaymentType;

    private String schemePaymentSubtype;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "accountName", column = @Column(name = "sponsorAccountName")),
            @AttributeOverride(name = "accountNumber", column = @Column(name = "sponsorAccountNumber")),
            @AttributeOverride(name = "accountNumberCode", column = @Column(name = "sponsorAccountNumberCode")),
            @AttributeOverride(name = "accountType", column = @Column(name = "sponsorAccountType")),
            @AttributeOverride(name = "address", column = @Column(name = "sponsorAddress")),
            @AttributeOverride(name = "bankId", column = @Column(name = "sponsorBankId")),
            @AttributeOverride(name = "bankIdCode", column = @Column(name = "sponsorBankIdCode")),
            @AttributeOverride(name = "name", column = @Column(name = "sponsorName"))
    })
    private Party sponsorParty;

}
