package com.benjvi.f3.rest;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class SenderPaymentChargeDTO {

    @JsonSerialize(using = ToStringSerializer.class)
    private String amount;

    private String currency;

}
