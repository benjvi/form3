package com.benjvi.f3.rest;

import com.benjvi.f3.entity.Payment;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class PaymentDTO {

    private String id;

    private String type;

    private Long version;

    @JsonProperty("organisation_id")
    private String organisationId;

    private PaymentAttributesDTO attributes;

    public static PaymentDTO fromAggregate(Payment aggregate) {
        ModelMapper modelMapper = new ModelMapper();
        PaymentDTO dto = modelMapper.map(aggregate, PaymentDTO.class);
        return dto;
    }

    public Payment toAggregate() {
        ModelMapper modelMapper = new ModelMapper();
        Payment domainModel = modelMapper.map(this, Payment.class);
        domainModel.getAttributes().getChargesInformation().getSenderCharges().stream().forEach(c -> c.setPayment(domainModel));
        return domainModel;
    }
}
