package com.benjvi.f3.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Data
public class PaymentChargesDTO {

    @JsonProperty("bearer_code")
    private String chargesBearerCode;

    @JsonProperty("receiver_charges_amount")
    private String receiverChargesAmount;

    @JsonProperty("receiver_charges_currency")
    private String receiverChargesCurrency;

    @JsonProperty("sender_charges")
    private List<SenderPaymentChargeDTO> senderCharges;

}
