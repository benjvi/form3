package com.benjvi.f3.rest;

import com.benjvi.f3.entity.PaymentAttributes;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.modelmapper.ModelMapper;

import java.util.Date;

@Data
public class PaymentAttributesDTO {

    @JsonSerialize(using = ToStringSerializer.class)
    private String amount;

    @JsonProperty("beneficiary_party")
    private PartyDTO beneficiaryParty;

    @JsonProperty("charges_information")
    private PaymentChargesDTO chargesInformation;

    private String currency;

    @JsonProperty("debtor_party")
    private PartyDTO debtorParty;

    @JsonProperty("end_to_end_reference")
    private String endToEndReference;

    private ForexDTO fx;

    @JsonProperty("numeric_reference")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long numericReference;

    @JsonProperty("payment_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long paymentId;

    @JsonProperty("payment_purpose")
    private String paymentPurpose;

    @JsonProperty("payment_scheme")
    private String paymentScheme; //TODO: enum

    @JsonProperty("payment_type")
    private String paymentType;   // TODO: enum

    @JsonProperty("processing_date")
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date processingDate; // assuming consistent timezones everywhere

    private String reference;

    @JsonProperty("scheme_payment_type")
    private String schemePaymentType; // could make a separate entity for scheme

    @JsonProperty("scheme_payment_sub_type")
    private String schemePaymentSubtype;

    @JsonProperty("sponsor_party")
    private PartyDTO sponsorParty;

    public static PaymentAttributesDTO fromAggregate(PaymentAttributes aggregate) {
        ModelMapper modelMapper = new ModelMapper();
        PaymentAttributesDTO dto = modelMapper.map(aggregate, PaymentAttributesDTO.class);
        return dto;
    }

    public PaymentAttributes toAggregate() {
        ModelMapper modelMapper = new ModelMapper();
        PaymentAttributes domainModel = modelMapper.map(this, PaymentAttributes.class);
        return domainModel;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PartyDTO getBeneficiaryParty() {
        return beneficiaryParty;
    }

    public void setBeneficiaryParty(PartyDTO beneficiaryParty) {
        this.beneficiaryParty = beneficiaryParty;
    }

    public PaymentChargesDTO getChargesInformation() {
        return chargesInformation;
    }

    public void setChargesInformation(PaymentChargesDTO chargesInformation) {
        this.chargesInformation = chargesInformation;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public PartyDTO getDebtorParty() {
        return debtorParty;
    }

    public void setDebtorParty(PartyDTO debtorParty) {
        this.debtorParty = debtorParty;
    }

    public String getEndToEndReference() {
        return endToEndReference;
    }

    public void setEndToEndReference(String endToEndReference) {
        this.endToEndReference = endToEndReference;
    }

    public ForexDTO getFx() {
        return fx;
    }

    public void setFx(ForexDTO fx) {
        this.fx = fx;
    }

    public Long getNumericReference() {
        return numericReference;
    }

    public void setNumericReference(Long numericReference) {
        this.numericReference = numericReference;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentPurpose() {
        return paymentPurpose;
    }

    public void setPaymentPurpose(String paymentPurpose) {
        this.paymentPurpose = paymentPurpose;
    }

    public String getPaymentScheme() {
        return paymentScheme;
    }

    public void setPaymentScheme(String paymentScheme) {
        this.paymentScheme = paymentScheme;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Date getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(Date processingDate) {
        this.processingDate = processingDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSchemePaymentType() {
        return schemePaymentType;
    }

    public void setSchemePaymentType(String schemePaymentType) {
        this.schemePaymentType = schemePaymentType;
    }

    public String getSchemePaymentSubtype() {
        return schemePaymentSubtype;
    }

    public void setSchemePaymentSubtype(String schemePaymentSubtype) {
        this.schemePaymentSubtype = schemePaymentSubtype;
    }

    public PartyDTO getSponsorParty() {
        return sponsorParty;
    }

    public void setSponsorParty(PartyDTO sponsorParty) {
        this.sponsorParty = sponsorParty;
    }
}
