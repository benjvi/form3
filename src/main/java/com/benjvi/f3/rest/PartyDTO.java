package com.benjvi.f3.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.persistence.Embeddable;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@JsonInclude(NON_NULL)
public class PartyDTO {

    @JsonProperty("account_name")
    private String accountName;

    @JsonProperty("account_number")
    private String accountNumber;

    @JsonProperty("account_number_code")
    private String accountNumberCode;

    @JsonProperty("account_type")
    private Long accountType;

    private String address;

    @JsonProperty("bank_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long bankId;

    @JsonProperty("bank_id_code")
    private String bankIdCode;

    private String name;
}
