package com.benjvi.f3.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ForexDTO {

    @JsonProperty("contract_reference")
    private String fxContractReference;

    @JsonProperty("exchange_rate")
    @JsonSerialize(using = ToStringSerializer.class)
    private String fxExchangeRate;

    @JsonProperty("original_amount")
    @JsonSerialize(using = ToStringSerializer.class)
    private String fxOriginalAmount;

    @JsonProperty("original_currency")
    private String fxOriginalCurrency; // ~enum type

 }
