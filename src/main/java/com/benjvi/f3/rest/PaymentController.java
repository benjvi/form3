package com.benjvi.f3.rest;

import com.benjvi.f3.entity.Payment;
import com.benjvi.f3.entity.PaymentAttributes;
import com.benjvi.f3.persistence.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

import static com.benjvi.f3.rest.PaymentDTO.fromAggregate;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.util.StringUtils.isEmpty;

@RestController
public class PaymentController {

    @Autowired
    private PaymentRepository paymentRepository;

    @RequestMapping(path="/payments/{paymentId}", method = RequestMethod.GET)
    public ResponseEntity<PaymentDTO> getPayment(@PathVariable String paymentId) {
        Payment payment = paymentRepository.findOne(paymentId);
        if (payment == null) {
            return notFound().build();
        }
        PaymentDTO respData = fromAggregate(payment);
        return ok(respData);
    }

    @RequestMapping(path="/payments/{paymentId}", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<PaymentDTO> updatePayment(@PathVariable String paymentId,
                                                    @RequestBody PaymentAttributesDTO updatedPayment) {
        // full update
        PaymentAttributes updatedAttributes = updatedPayment.toAggregate();

        Payment existingPayment = paymentRepository.findOne(paymentId);

        Payment newPayment = new Payment(updatedAttributes, existingPayment.getOrganisationId());
        newPayment.setId(existingPayment.getId());
        newPayment.setVersion(existingPayment.getVersion()+1);

        PaymentDTO respData = fromAggregate(newPayment);
        return ok(respData);
    }

    @RequestMapping(path="/payments/{paymentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePayment(@PathVariable String paymentId) {
        try {
            paymentRepository.delete(paymentId);
        } catch (EmptyResultDataAccessException e) {
            return notFound().build();
        }

        return ok(null);
    }

    @RequestMapping(path="/payments", method = RequestMethod.GET)
    public ResponseEntity<PaymentCollectionDTO> getAllPayments() {
        List<PaymentDTO> payments = paymentRepository.findAll().stream()
                .map(PaymentDTO::fromAggregate)
                .collect(toList());

        return ok(new PaymentCollectionDTO(payments));
    }

    @RequestMapping(path="/payments",method = RequestMethod.POST)
    public ResponseEntity<PaymentDTO> createPayment(@RequestHeader(value = "Authentication") String authentication,
                                                    @RequestBody PaymentAttributesDTO createPayment) {
        if (isEmpty(authentication) || !authentication.startsWith("Bearer ")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        } else {
            PaymentAttributes attributes = createPayment.toAggregate();

            Payment payment = new Payment(attributes, authentication.substring(7));
            paymentRepository.save(payment);

            return ok(PaymentDTO.fromAggregate(payment));
        }
    }



}
