package com.benjvi.f3.rest;

import lombok.Data;

import java.util.List;

@Data
public class PaymentCollectionDTO {

    public PaymentCollectionDTO() {
    }

    public PaymentCollectionDTO(List<PaymentDTO> payments) {
        this.payments = payments;
    }

    private List<PaymentDTO> payments;

}
