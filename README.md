# Payments Management Service

## Tests

Startup postgres in docker container like: 

`docker run --name questionmarks-psql \                                        
    -p 5432:5432 \
    -e POSTGRES_DB=form3 \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -d postgres`

Then run tests with gradle: `gradle test`

## Run

`gradle bootRun`

Starts on port 8080 by default
